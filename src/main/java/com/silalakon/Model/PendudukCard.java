package com.silalakon.Model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Date;


@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "PENDUDUK_CARD")
public class PendudukCard {

    @Transient
    private static final String SEQEUNCE_NAME = "user_sequence";

    @Id
    private ObjectId id;

    private String desa;
    private String noktp;
    private String nama;
    private String gender;
    private String tempatLahir;
    private Date tglLahir;
    private String citizen;
    private String statusDalamKeluarga;
    private String alamat;
    private String kategoriCard;
    private String keteranganKategori;
    private Date tglCard;
    private LocalDateTime tglSystem;
    private String tglCreated;
    private Integer count;

}
