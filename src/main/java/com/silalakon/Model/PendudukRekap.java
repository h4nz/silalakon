package com.silalakon.Model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "PENDUDUK_REKAP")
public class PendudukRekap {
    @Id
    private ObjectId id;

    private String bulanRekap;
    private String desa;
    private String citizen;
    private Integer awal_l;
    private Integer awal_p;
    private Integer lahir_l;
    private Integer lahir_p;
    private Integer meninggal_l;
    private Integer meninggal_p;
    private Integer pindah_l;
    private Integer pindah_p;
    private Integer datang_l;
    private Integer datang_p;
    private Integer akhir_l;
    private Integer akhir_p;
}
