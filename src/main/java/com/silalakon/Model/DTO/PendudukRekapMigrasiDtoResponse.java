package com.silalakon.Model.DTO;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PendudukRekapMigrasiDtoResponse {
    private String id;
    private String bulanRekap;
    private String desa;
    private String citizen;
    private String kategoriPeristiwa;
    private Integer migrasiMasukDLN_L;
    private Integer migrasiMasukDLN_P;
    private Integer migrasiMasukDLN_JML;
    private Integer migrasiMasukLRN_L;
    private Integer migrasiMasukLRN_P;
    private Integer migrasiMasukLRN_JML;
    private Integer migrasiKeluarDLN_L;
    private Integer migrasiKeluarDLN_P;
    private Integer migrasiKeluarDLN_JML;
    private Integer migrasiKeluarLRN_L;
    private Integer migrasiKeluarLRN_P;
    private Integer migrasiKeluarLRN_JML;
    private Integer jmlMasuk;
    private Integer jmlKeluar;
}
