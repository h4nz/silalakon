package com.silalakon.Model.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;
import java.util.Date;


@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PendudukCardDtoResponse {

    private String id;
    private String desa;
    private String noktp;
    private String nama;
    private String gender;
    private String tempatLahir;
    private Date tglLahir;
    private String citizen;
    private String statusDalamKeluarga;
    private String alamat;
    private String kategoriCard;
    private String keteranganKategori;
    private String tglCreated;
    private String tglLahirDb;
    private Integer currentAge;
    private Integer count;
}
