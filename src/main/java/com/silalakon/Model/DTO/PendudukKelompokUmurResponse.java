package com.silalakon.Model.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PendudukKelompokUmurResponse {
    String desa;

    Integer empatL;
    Integer empatLP;
    Integer empatJ;

    Integer limaL;
    Integer limaP;
    Integer limaJ;

    Integer tenL;
    Integer tenP;
    Integer tenJ;

    Integer fifteenL;
    Integer fifteenP;
    Integer fifteenJ;

    Integer twentyL;
    Integer twentyP;
    Integer twentyJ;

    Integer duaLimaL;
    Integer duaLimaP;
    Integer duaLimaJ;

    Integer tigaempatL;
    Integer tigaempatP;
    Integer tigaempatJ;

    Integer tigalimaL;
    Integer tigalimaP;
    Integer tigalimaJ;

    Integer fourtyL;
    Integer fourtyP;
    Integer fourtyJ;

    Integer empatLimaL;
    Integer empatLimaP;
    Integer empatLimaJ;

    Integer fivetyL;
    Integer fivetyP;
    Integer fivetyJ;

    Integer limalimaL;
    Integer limalimaP;
    Integer limalimaJ;

    Integer sixtyL;
    Integer sixtyP;
    Integer sixtyJ;
}
