package com.silalakon.Model.DTO;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PendudukRekapDtoResponse {
    private String id;
    private String bulanRekap;
    private String desa;
    private String citizen;
    private Integer awal_l;
    private Integer awal_p;
    private Integer awal_jml;
    private Integer lahir_l;
    private Integer lahir_p;
    private Integer lahir_jml;
    private Integer meninggal_l;
    private Integer meninggal_p;
    private Integer meninggal_jml;
    private Integer pindah_l;
    private Integer pindah_p;
    private Integer pindah_jml;
    private Integer datang_l;
    private Integer datang_p;
    private Integer datang_jml;
    private Integer akhir_l;
    private Integer akhir_p;
    private Integer akhir_jml;

}
