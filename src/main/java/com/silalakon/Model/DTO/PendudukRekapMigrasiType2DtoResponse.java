package com.silalakon.Model.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PendudukRekapMigrasiType2DtoResponse {
    private String citizen;
    private String kategori;
    private Integer januari;
    private Integer februari;
    private Integer maret;
    private Integer april;
    private Integer mei;
    private Integer juni;
    private Integer juli;
    private Integer agustus;
    private Integer september;
    private Integer oktober;
    private Integer november;
    private Integer desember;
    private Integer total;
    private String ket;

}
