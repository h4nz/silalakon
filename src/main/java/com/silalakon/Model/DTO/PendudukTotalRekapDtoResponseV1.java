package com.silalakon.Model.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PendudukTotalRekapDtoResponseV1 {

    private String bulanRekap;
    private String desa;
    private Integer wni_L;
    private Integer wni_P;
    private Integer wni_JML;
    private Integer wna_L;
    private Integer wna_P;
    private Integer wna_JML;
    private Integer subTotal_L;
    private Integer subTotal_P;
    private Integer grandTotal;
}
