package com.silalakon.Model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "PENDUDUK_REKAP_MIGRASI")
public class PendudukRekapMigrasi {

    @Transient
    private static final String SEQEUNCE_NAME = "user_sequence";

    @Id
    private ObjectId id;

    private String desa;
    private String citizen;

    private Integer migrasiMasukDln_l;
    private Integer migrasiMasukDln_p;
    private Integer migrasiMasukLrn_l;
    private Integer migrasiMasukLrn_p;

    private Integer migrasiMKeluarDln_l;
    private Integer migrasiKeluarDln_p;
    private Integer migrasiKeluarLrn_l;
    private Integer migrasiKeluarLrn_p;
}
