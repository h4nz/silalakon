package com.silalakon.Services;

import com.silalakon.Model.DTO.PendudukCardDtoResponse;
import com.silalakon.Model.DTO.PendudukKelompokUmurResponse;
import com.silalakon.Model.DTO.PendudukRekapMigrasiDtoResponse;
import com.silalakon.Model.DTO.PendudukRekapMigrasiType2DtoResponse;
import com.silalakon.Model.PendudukCard;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface PendudukCardService {

    PendudukCard sumbitData(PendudukCard data);
    PendudukCard removeData(Map<String, String> params);

    List<PendudukCardDtoResponse> findAllData(Pageable pageable, Map<String, String> params);
    List<PendudukRekapMigrasiDtoResponse> findByDateRekapMigrasi (Pageable pageable, Map<String, String> params);
    List<PendudukRekapMigrasiType2DtoResponse> findByDateRekapMigrasiType2 (Pageable pageable, Map<String, String> params);
    List<PendudukKelompokUmurResponse> findAllDataRekapByAgeCategory(Map<String, String> params);
}
