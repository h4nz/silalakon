package com.silalakon.Services;

import com.silalakon.Model.DTO.PendudukRekapDtoResponse;
import com.silalakon.Model.DTO.PendudukTotalRekapDtoResponseV1;
import com.silalakon.Model.PendudukCard;
import com.silalakon.Model.PendudukRekap;
import com.silalakon.Repository.PendudukCardDaoService;
import com.silalakon.Repository.PendudukRekapDaoService;
import com.silalakon.Repository.PendudukRekapRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.silalakon.Constant.CommonParam.*;

@Service
@Transactional
public class PendudukRekapServiceImpl implements  PendudukRekapServices{

    @Autowired
    PendudukRekapDaoService rekapDaoService;

    @Autowired
    PendudukCardDaoService pendudukCardService;


    @Override
    public PendudukRekap submitData(PendudukCard data) {

        PendudukRekap rekap = new PendudukRekap();

        //1. Find Data in Rekap by Date of Month, Village and Citizen
        Optional<PendudukRekap> optRekap = rekapDaoService.findByDateDesaCitizen(mapperDateToPattern(data.getTglCard()),data.getDesa(), data.getCitizen());
        if(optRekap.isPresent()){
            rekap = mapperFromPendudukCard(data,optRekap.get());
            rekap.setId(optRekap.get().getId());
        }else {
            rekap = mapperFromPendudukCard(data,new PendudukRekap());
        }

        rekapDaoService.submit(rekap);
        return rekap;
    }

    @Override
    public PendudukRekap rollbackData(PendudukCard data) {

        PendudukRekap rekap = new PendudukRekap();
        Optional<PendudukRekap> optRekap = rekapDaoService.findByDateDesaCitizen(mapperDateToPattern(data.getTglCard()),data.getDesa(), data.getCitizen());

        rekap = mapperFromPendudukCardForRemove(data,optRekap.get());
        rekap.setId(optRekap.get().getId());
        rekapDaoService.submit(rekap);

        return null;
    }

    @Override
    public List<PendudukRekapDtoResponse> findByDate(Pageable pageable, Map<String, String> params) {

        List<PendudukRekap> listDataSaldoAwal = new ArrayList<>();
        List<PendudukRekapDtoResponse> listResult = new ArrayList<>();
        List<String> monthListAwal = new ArrayList<>();
        List<String> monthList      = new ArrayList<>();
        List<String> month2 = new ArrayList<>();
        PendudukRekap rekapTmp = new PendudukRekap();


        String month = params.get("month");
        String citizen = params.get("citizen");
        String desa = params.get("desa");
        String staticMonth = "2024-02";

        if (month == null){
            return null;
        }

        month = getCurrenYear() + "-" + mapperForMonthToNumber(params.get("month"));
        monthList = listMonthFilter(mapperForMonthToNumber(params.get("month")));
        monthListAwal = listAwalMonthFilter(mapperForMonthToNumber(params.get("month")));
        month2.add(month);

        if(month != null && citizen != null){
            listDataSaldoAwal = rekapDaoService.findAllDataByMonthCitizen(staticMonth,citizen);
        }else {
            listDataSaldoAwal = rekapDaoService.findAllDataByMonth(staticMonth);
        }

        listResult = listDataSaldoAwal.stream().map(this::mapperFromEntityToDto).collect(Collectors.toList());

        //This Part For get saldo awal, in this case we are get from saldo akhir 2024-02
        System.out.println(monthList);
        System.out.println(monthListAwal);
        for(PendudukRekapDtoResponse response :  listResult){

            //This Part For Add Saldo Awal
            if(!month.equalsIgnoreCase("2024-03")){
                List<PendudukCard> countTmpRekap = pendudukCardService.findAllDataThatAliveByVillageCitizenV2(
                        monthListAwal,response.getDesa(),response.getCitizen());

                List<PendudukCard> countTmpRekapLeave = pendudukCardService.findAllDataThatLeaveByVillageCitizenV2(
                        monthListAwal,response.getDesa(),response.getCitizen());

                Integer countL = countTmpRekap.stream()
                        .filter(data -> data.getGender().equalsIgnoreCase("L"))
                        .map(x -> x.getCount())
                        .reduce(0, Integer::sum);

                Integer countP = countTmpRekap.stream()
                        .filter(data -> data.getGender().equalsIgnoreCase("p"))
                        .map(x -> x.getCount())
                        .reduce(0, Integer::sum);

                Integer countLSub = countTmpRekapLeave.stream()
                        .filter(data -> data.getGender().equalsIgnoreCase("L"))
                        .map(x -> x.getCount())
                        .reduce(0, Integer::sum);

                Integer countPSub = countTmpRekapLeave.stream()
                        .filter(data -> data.getGender().equalsIgnoreCase("p"))
                        .map(x -> x.getCount())
                        .reduce(0, Integer::sum);

                countL = countL - countLSub;
                countP = countP - countPSub;

//                if(response.getDesa().equalsIgnoreCase("JATIMULYA")){
//                    System.out.println( "AWAL L  " + response.getAkhir_l() + " count prev " + countL + " Count SUB " + countLSub);
//                }

                response.setAwal_p(response.getAkhir_p() + countP);
                response.setAwal_l(response.getAkhir_l() + countL);
                response.setAwal_jml( response.getAwal_l() + response.getAwal_p());
            }else{
                response.setAwal_p(response.getAkhir_p());
                response.setAwal_l(response.getAkhir_l());
                response.setAwal_jml( response.getAwal_l() + response.getAwal_p());
            }

            //This Part For search
            Optional <PendudukRekap> rekapOpt = rekapDaoService.findByDateDesaCitizen(month,response.getDesa(),response.getCitizen());
            List<PendudukCard> cardInputDb  = pendudukCardService.findAllDataOneMonthDesa(month,response.getDesa());

            if(rekapOpt.isPresent()){
                PendudukRekap rekap = rekapOpt.get();
                response.setLahir_l(cardInputDb.stream()
                        .filter(data -> data.getGender().equalsIgnoreCase("L")
                        && data.getKategoriCard().equalsIgnoreCase("LAHIR")
                        && data.getCitizen().equalsIgnoreCase(response.getCitizen()))
                        .map(x -> x.getCount())
                        .reduce(0, Integer::sum));
                response.setLahir_p(cardInputDb.stream()
                        .filter(data -> data.getGender().equalsIgnoreCase("P")
                                && data.getKategoriCard().equalsIgnoreCase("LAHIR")
                                && data.getCitizen().equalsIgnoreCase(response.getCitizen()))
                        .map(x -> x.getCount())
                        .reduce(0, Integer::sum));
                response.setLahir_jml(response.getLahir_l() + response.getLahir_p());

                response.setMeninggal_l(cardInputDb.stream()
                        .filter(data -> data.getGender().equalsIgnoreCase("L")
                                && data.getKategoriCard().equalsIgnoreCase("MATI")
                                && data.getCitizen().equalsIgnoreCase(response.getCitizen()))
                        .map(x -> x.getCount())
                        .reduce(0, Integer::sum));
                response.setMeninggal_p(cardInputDb.stream()
                        .filter(data -> data.getGender().equalsIgnoreCase("P")
                                && data.getKategoriCard().equalsIgnoreCase("MATI")
                                && data.getCitizen().equalsIgnoreCase(response.getCitizen()))
                        .map(x -> x.getCount())
                        .reduce(0, Integer::sum));
                response.setMeninggal_jml(response.getMeninggal_l() + response.getMeninggal_p());

                response.setPindah_l(cardInputDb.stream()
                        .filter(data -> data.getGender().equalsIgnoreCase("L")
                                && data.getKategoriCard().equalsIgnoreCase("PINDAH")
                                && data.getCitizen().equalsIgnoreCase(response.getCitizen()))
                        .map(x -> x.getCount())
                        .reduce(0, Integer::sum));
                response.setPindah_p(cardInputDb.stream()
                        .filter(data -> data.getGender().equalsIgnoreCase("P")
                                && data.getKategoriCard().equalsIgnoreCase("PINDAH")
                                && data.getCitizen().equalsIgnoreCase(response.getCitizen()))
                        .map(x -> x.getCount())
                        .reduce(0, Integer::sum));
                response.setPindah_jml(response.getPindah_l()+ response.getPindah_p());

                response.setDatang_l(cardInputDb.stream()
                        .filter(data -> data.getGender().equalsIgnoreCase("L")
                                && data.getKategoriCard().equalsIgnoreCase("DATANG")
                                && data.getCitizen().equalsIgnoreCase(response.getCitizen()))
                        .map(x -> x.getCount())
                        .reduce(0, Integer::sum));
                response.setDatang_p(cardInputDb.stream()
                        .filter(data -> data.getGender().equalsIgnoreCase("P")
                                && data.getKategoriCard().equalsIgnoreCase("DATANG")
                                && data.getCitizen().equalsIgnoreCase(response.getCitizen()))
                        .map(x -> x.getCount())
                        .reduce(0, Integer::sum));
                response.setDatang_jml(response.getDatang_l()+response.getDatang_p());
            }

            response.setAkhir_l(response.getAwal_l() + response.getLahir_l() + response.getDatang_l() - response.getMeninggal_l() - response.getPindah_l());
            response.setAkhir_p(response.getAwal_p() + response.getLahir_p() + response.getDatang_p() - response.getMeninggal_p() - response.getPindah_p());
            response.setAkhir_jml(response.getAkhir_p() + response.getAkhir_l());
        }

        return listResult;
    }

    @Override
    public List<PendudukTotalRekapDtoResponseV1> findRekapTotalPendudukByDate(Pageable pageable, Map<String, String> params) {
        List<PendudukTotalRekapDtoResponseV1> listResult = new ArrayList<>();
        List<String> monthFilter = new ArrayList<>();
        List<PendudukRekap> listDb = new ArrayList<>();
        List<PendudukRekap> rekapTmp = new ArrayList<>();



        String staticMonth = "2024-02";
        String month;

        month = getCurrenYear() + "-" + mapperForMonthToNumber(params.get("month"));
        if( params.get("month") == null){
            return null;
        }

        List<String>    daftarDesa = getListDesa();
        rekapTmp = rekapDaoService.findAllDataByMonth(staticMonth); //this part for get awal balance
        monthFilter = listMonthFilter (mapperForMonthToNumber(params.get("month")));

        System.out.println("");

        for (String desa : daftarDesa){
            PendudukTotalRekapDtoResponseV1 dtoResponse = new PendudukTotalRekapDtoResponseV1();

            //This Part Filder Saldo Awal By Citizen
            PendudukRekap rekapSaldoAwalWni = rekapTmp.stream().filter(data -> data.getDesa().equalsIgnoreCase(desa)
                    && data.getCitizen().equalsIgnoreCase("WNI"))
                    .findAny()
                    .orElse(null);

            PendudukRekap rekapSaldoAwalWna = rekapTmp.stream().filter(data -> data.getDesa().equalsIgnoreCase(desa)
                    && data.getCitizen().equalsIgnoreCase("WNA"))
                    .findAny()
                    .orElse(null);

            //This Part Filder Card Awal By Citizen
            List<PendudukCard> cardRekapWni = pendudukCardService.findAllDataThatAliveByVillageCitizenV2(monthFilter,desa,"WNI");
            List<PendudukCard> cardRekapWna = pendudukCardService.findAllDataThatAliveByVillageCitizenV2(monthFilter,desa,"WNA");

            List<PendudukCard> cardSubWni = pendudukCardService.findAllDataThatLeaveByVillageCitizenV2(monthFilter,desa,"WNI");
            List<PendudukCard> cardSubWna = pendudukCardService.findAllDataThatLeaveByVillageCitizenV2(monthFilter,desa,"WNA");

            Integer countWniL = rekapSaldoAwalWni.getAkhir_l() ;
            Integer countWniP  = rekapSaldoAwalWni.getAkhir_p();

            Integer countWnaL = (rekapSaldoAwalWna != null ? rekapSaldoAwalWna.getAkhir_l():0);
            Integer countWnaP =(rekapSaldoAwalWna != null ? rekapSaldoAwalWna.getAkhir_p():0);


            //This part For Add
            countWniL = countWniL + (cardRekapWni.stream()
                    .filter(data -> data.getGender().equalsIgnoreCase("L"))
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));

            countWniP = countWniP + (cardRekapWni.stream()
                    .filter(data -> data.getGender().equalsIgnoreCase("P"))
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));

            countWnaL = countWnaL + (cardRekapWna.stream()
                    .filter(data -> data.getGender().equalsIgnoreCase("L"))
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));

            countWnaP = countWnaP + (cardRekapWna.stream()
                    .filter(data -> data.getGender().equalsIgnoreCase("P"))
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));

            //This Part For Sub
            countWniL = countWniL - (cardSubWni.stream()
                    .filter(data -> data.getGender().equalsIgnoreCase("L"))
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            countWniP = countWniP - (cardSubWni.stream()
                    .filter(data -> data.getGender().equalsIgnoreCase("P"))
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));

            countWnaL = countWnaL - (cardSubWna.stream()
                    .filter(data -> data.getGender().equalsIgnoreCase("L"))
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            countWnaP = countWnaP - (cardSubWna.stream()
                    .filter(data -> data.getGender().equalsIgnoreCase("L"))
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));

            dtoResponse.setDesa(desa);
            dtoResponse.setWni_L(countWniL);
            dtoResponse.setWni_P(countWniP);

            dtoResponse.setWna_L(countWnaL);
            dtoResponse.setWna_P(countWnaP);

            dtoResponse.setWni_JML(dtoResponse.getWni_L() + dtoResponse.getWni_P());
            dtoResponse.setWna_JML(dtoResponse.getWna_L() + dtoResponse.getWna_P());

            dtoResponse.setSubTotal_L(dtoResponse.getWni_L() + dtoResponse.getWna_L());
            dtoResponse.setSubTotal_P(dtoResponse.getWni_P() + dtoResponse.getWna_P());

            dtoResponse.setGrandTotal(dtoResponse.getWni_JML() + dtoResponse.getWna_JML());

            listResult.add(dtoResponse);
        }

        return listResult;
    }

    private PendudukRekapDtoResponse mapperFromEntityToDto(PendudukRekap data){
        PendudukRekapDtoResponse response = new PendudukRekapDtoResponse();

        response.setId(String.valueOf(data.getId()));
        response.setDesa(data.getDesa());
        response.setCitizen(data.getCitizen());
        response.setBulanRekap(data.getBulanRekap());

        response.setAwal_l(data.getAwal_l());
        response.setAwal_p(data.getAwal_p());
        response.setAwal_jml(data.getAwal_l() + data.getAwal_p());

        response.setLahir_l(data.getLahir_l());
        response.setLahir_p(data.getLahir_p());
        response.setLahir_jml(data.getLahir_p() + data.getLahir_l());

        response.setMeninggal_l(data.getMeninggal_l());
        response.setMeninggal_p(data.getMeninggal_p());
        response.setMeninggal_jml(data.getMeninggal_l() + data.getMeninggal_p());

        response.setPindah_l(data.getPindah_l());
        response.setPindah_p(data.getPindah_p());
        response.setPindah_jml(data.getPindah_p() + data.getPindah_l());

        response.setDatang_l(data.getDatang_l());
        response.setDatang_p(data.getDatang_p());
        response.setDatang_jml(data.getDatang_l() + data.getDatang_p());

        response.setAkhir_l(data.getAkhir_l());
        response.setAkhir_p(data.getAkhir_p());
        response.setAkhir_jml(data.getAkhir_p() + data.getAkhir_l());

        return response;
    }

    private PendudukRekapDtoResponse mapperFromEntityToDtoAwalBalance(PendudukRekap data){
        PendudukRekapDtoResponse response = new PendudukRekapDtoResponse();

        response.setId(String.valueOf(data.getId()));
        response.setDesa(data.getDesa());
        response.setCitizen(data.getCitizen());
        response.setBulanRekap(data.getBulanRekap());

        response.setAwal_l(data.getAkhir_l());
        response.setAwal_p(data.getAkhir_p());
        response.setAwal_jml(data.getAkhir_l() + data.getAkhir_p());

        response.setLahir_l(0);
        response.setLahir_p(0);
        response.setLahir_jml(0);

        response.setMeninggal_l(0);
        response.setMeninggal_p(0);
        response.setMeninggal_jml(0);

        response.setPindah_l(0);
        response.setPindah_p(0);
        response.setPindah_jml(0);

        response.setDatang_l(0);
        response.setDatang_p(0);
        response.setDatang_jml(0);

        response.setAkhir_l(data.getAkhir_l());
        response.setAkhir_p(data.getAkhir_p());
        response.setAkhir_jml(data.getAkhir_p() + data.getAkhir_l());

        return response;
    }

    private PendudukRekap mapperFromPendudukCardForRemove(PendudukCard data, PendudukRekap rekapDb){
        PendudukRekap rekap = new PendudukRekap();

        rekap.setBulanRekap(mapperDateToPattern(data.getTglCard()));
        rekap.setDesa(data.getDesa());
        rekap.setCitizen(data.getCitizen());

        if (rekapDb.getId() != null){
            rekap.setAwal_l(rekapDb.getAwal_l());
            rekap.setAwal_p(rekapDb.getAwal_p());

            rekap.setLahir_l(rekapDb.getLahir_l());
            rekap.setLahir_p(rekapDb.getLahir_p());
            rekap.setMeninggal_l(rekapDb.getMeninggal_l());
            rekap.setMeninggal_p(rekapDb.getMeninggal_p());
            rekap.setPindah_l(rekapDb.getPindah_l());
            rekap.setPindah_p(rekapDb.getPindah_p());
            rekap.setDatang_l(rekapDb.getDatang_l());
            rekap.setDatang_p(rekapDb.getDatang_p());

            rekap.setAkhir_l(rekapDb.getAkhir_l());
            rekap.setAkhir_p(rekapDb.getAkhir_p());

        }else{
            rekap.setAwal_l(0);
            rekap.setAwal_p(0);
            rekap.setLahir_l(0);
            rekap.setLahir_p(0);
            rekap.setMeninggal_l(0);
            rekap.setMeninggal_p(0);
            rekap.setPindah_l(0);
            rekap.setPindah_p(0);
            rekap.setDatang_l(0);
            rekap.setDatang_p(0);
            rekap.setAkhir_l(0);
            rekap.setAkhir_p(0);
        }

        switch (data.getKategoriCard()){
            case PENDUDUK_CARD_CATEGORY_LAHIR:
                switch (data.getGender()){
                    case GENDER_LAKI:
                        rekap.setLahir_l((rekapDb.getLahir_l() == null ? 0  : rekapDb.getLahir_l() ) - 1);
                        rekap.setAkhir_l((rekapDb.getAkhir_l() == null ? 0  : rekapDb.getAkhir_l() ) - 1);
                        break;
                    case GENDER_WANITA:
                        rekap.setLahir_p((rekapDb.getLahir_p() == null ? 0  : rekapDb.getLahir_p())  - 1);
                        rekap.setAkhir_p((rekapDb.getAkhir_p() == null ? 0  : rekapDb.getAkhir_p() ) - 1);
                        break;
                }
                break;
            case PENDUDUK_CARD_CATEGORY_MATI:
                switch (data.getGender()){
                    case GENDER_LAKI:
                        rekap.setMeninggal_l((rekapDb.getMeninggal_l() == null ? 0 : rekapDb.getMeninggal_l()) - 1);
                        rekap.setAkhir_l((rekapDb.getAkhir_l() == null ? 0  : rekapDb.getAkhir_l() ) + 1);
                        break;
                    case GENDER_WANITA:
                        rekap.setMeninggal_p((rekapDb.getMeninggal_p() == null ? 9 : rekapDb.getMeninggal_p()) - 1);
                        rekap.setAkhir_p((rekapDb.getAkhir_p() == null ? 0  : rekapDb.getAkhir_p()) + 1);
                        break;
                }
                break;
            case PENDUDUK_CARD_CATEGORY_PINDAH:
                switch (data.getGender()){
                    case GENDER_LAKI:
                        rekap.setPindah_l((rekapDb.getPindah_l() == null ? 0 : rekapDb.getPindah_l()) - 1);
                        rekap.setAkhir_l((rekapDb.getAkhir_l() == null ? 0  : rekapDb.getAkhir_l()) + 1);
                        break;
                    case GENDER_WANITA:
                        rekap.setPindah_p((rekapDb.getPindah_p() == null ? 0 : rekapDb.getPindah_p()) - 1);
                        rekap.setAkhir_p((rekapDb.getAkhir_p() == null ? 0  : rekapDb.getAkhir_p()) + 1);
                        break;
                }
                break;
            case PENDUDUK_CARD_CATEGORY_DATANG:
                switch (data.getGender()){
                    case GENDER_LAKI:
                        rekap.setDatang_l((rekapDb.getDatang_l() == null ? 0: rekapDb.getDatang_l()) - 1);
                        rekap.setAkhir_l((rekapDb.getAkhir_l() == null ? 0  : rekapDb.getAkhir_l() ) - 1);
                        break;
                    case GENDER_WANITA:
                        rekap.setDatang_p((rekapDb.getDatang_p() == null ? 0 : rekapDb.getDatang_p()) - 1);
                        rekap.setAkhir_p((rekapDb.getAkhir_p() == null ? 0  : rekapDb.getAkhir_p() ) - 1);
                        break;
                }
                break;
        }

        return rekap;
    }

    private PendudukRekap mapperFromPendudukCard(PendudukCard data, PendudukRekap rekapDb){
        PendudukRekap rekap = new PendudukRekap();

        rekap.setBulanRekap(mapperDateToPattern(data.getTglCard()));
        rekap.setDesa(data.getDesa());
        rekap.setCitizen(data.getCitizen());

        if (rekapDb.getId() != null){
            rekap.setAwal_l(rekapDb.getAwal_l());
            rekap.setAwal_p(rekapDb.getAwal_p());

            rekap.setLahir_l(rekapDb.getLahir_l());
            rekap.setLahir_p(rekapDb.getLahir_p());
            rekap.setMeninggal_l(rekapDb.getMeninggal_l());
            rekap.setMeninggal_p(rekapDb.getMeninggal_p());
            rekap.setPindah_l(rekapDb.getPindah_l());
            rekap.setPindah_p(rekapDb.getPindah_p());
            rekap.setDatang_l(rekapDb.getDatang_l());
            rekap.setDatang_p(rekapDb.getDatang_p());

            rekap.setAkhir_l(rekapDb.getAkhir_l());
            rekap.setAkhir_p(rekapDb.getAkhir_p());

        }else{
            rekap.setAwal_l(0);
            rekap.setAwal_p(0);
            rekap.setLahir_l(0);
            rekap.setLahir_p(0);
            rekap.setMeninggal_l(0);
            rekap.setMeninggal_p(0);
            rekap.setPindah_l(0);
            rekap.setPindah_p(0);
            rekap.setDatang_l(0);
            rekap.setDatang_p(0);
            rekap.setAkhir_l(0);
            rekap.setAkhir_p(0);
        }

        switch (data.getKategoriCard()){
            case PENDUDUK_CARD_CATEGORY_LAHIR:
                switch (data.getGender()){
                    case GENDER_LAKI:
                        rekap.setLahir_l((rekapDb.getLahir_l() == null ? 0  : rekapDb.getLahir_l() ) + 1);
                        rekap.setAkhir_l((rekapDb.getAkhir_l() == null ? 0  : rekapDb.getAkhir_l() ) + 1);
                        break;
                    case GENDER_WANITA:
                        rekap.setLahir_p((rekapDb.getLahir_p() == null ? 0  : rekapDb.getLahir_p())  + 1);
                        rekap.setAkhir_p((rekapDb.getAkhir_p() == null ? 0  : rekapDb.getAkhir_p() ) + 1);
                        break;
                }
                break;
            case PENDUDUK_CARD_CATEGORY_MATI:
                switch (data.getGender()){
                    case GENDER_LAKI:
                        rekap.setMeninggal_l((rekapDb.getMeninggal_l() == null ? 0 : rekapDb.getMeninggal_l()) + 1);
                        rekap.setAkhir_l((rekapDb.getAkhir_l() == null ? 0  : rekapDb.getAkhir_l() ) - 1);
                        break;
                    case GENDER_WANITA:
                        rekap.setMeninggal_p((rekapDb.getMeninggal_p() == null ? 9 : rekapDb.getMeninggal_p()) + 1);
                        rekap.setAkhir_p((rekapDb.getAkhir_p() == null ? 0  : rekapDb.getAkhir_p()) - 1);
                        break;
                }
                break;
            case PENDUDUK_CARD_CATEGORY_PINDAH:
                switch (data.getGender()){
                    case GENDER_LAKI:
                        rekap.setPindah_l((rekapDb.getPindah_l() == null ? 0 : rekapDb.getPindah_l()) + 1);
                        rekap.setAkhir_l((rekapDb.getAkhir_l() == null ? 0  : rekapDb.getAkhir_l()) - 1);
                        break;
                    case GENDER_WANITA:
                        rekap.setPindah_p((rekapDb.getPindah_p() == null ? 0 : rekapDb.getPindah_p()) + 1);
                        rekap.setAkhir_p((rekapDb.getAkhir_p() == null ? 0  : rekapDb.getAkhir_p()) - 1);
                        break;
                }
                break;
            case PENDUDUK_CARD_CATEGORY_DATANG:
                switch (data.getGender()){
                    case GENDER_LAKI:
                        rekap.setDatang_l((rekapDb.getDatang_l() == null ? 0: rekapDb.getDatang_l()) + 1);
                        rekap.setAkhir_l((rekapDb.getAkhir_l() == null ? 0  : rekapDb.getAkhir_l() ) + 1);
                        break;
                    case GENDER_WANITA:
                        rekap.setDatang_p((rekapDb.getDatang_p() == null ? 0 : rekapDb.getDatang_p()) + 1);
                        rekap.setAkhir_p((rekapDb.getAkhir_p() == null ? 0  : rekapDb.getAkhir_p() ) + 1);
                        break;
                }
                break;
        }

        return rekap;
    }


    private String mapperForMonthToNumber(String month){
        String result="";

        switch(month){
            case "Januari":
                result = "01";
                break;
            case"Februari":
                result = "02";
                break;
            case "Maret":
                result = "03";
                break;
            case "April":
                result = "04";
                break;
            case "Mei":
                result = "05";
                break;
            case "Juni":
                result = "06";
                break;
            case "Juli":
                result = "07";
                break;
            case "Agustus":
                result = "08";
                break;
            case "September":
                result = "09";
                break;
            case "Oktober":
                result = "10";
                break;
            case "November":
                result = "11";
                break;
            case"Desember":
                result = "12";
                break;
        }

        return result;
    }

    private String mapperDateToPattern(Date date){
        String modifiedDate= new SimpleDateFormat("yyyy-MM").format(date);

        return  modifiedDate;
    }

    private String getCurrenBulanDate(){
        Date date = new Date();
        String modifiedDate= new SimpleDateFormat("yyyy-MM").format(date);

        return  modifiedDate;
    }

    private String getCurrenYear(){
        Date date = new Date();
        String modifiedDate= new SimpleDateFormat("yyyy").format(date);

        return  modifiedDate;
    }

    private void mapperListToMap(List<PendudukRekapDtoResponse> list){
        Map<String, String> map = list.stream()
                .collect(Collectors
                        .toMap(
                                PendudukRekapDtoResponse::getDesa,
                                PendudukRekapDtoResponse::getBulanRekap,
                                (oldValue, newValue) -> oldValue
                        ));

        System.out.println(String.valueOf(map));
    }

    private List<String> getListDesa(){
        List<String> daftarDesa = new ArrayList<>();
        daftarDesa.add("COMPRENG");
        daftarDesa.add("MEKARJAYA");
        daftarDesa.add("KALENSARI");
        daftarDesa.add("JATIREJA");
        daftarDesa.add("KIARASARI");
        daftarDesa.add("SUKATANI");
        daftarDesa.add("SUKADANA");
        daftarDesa.add("JATIMULYA");

        return daftarDesa;
    }

    private List<String> listMonthFilter(String month){
        List<String> monthList = new ArrayList<>();

        if(month != null){
            Integer monthVal = Integer.valueOf(month);
            for(monthVal=monthVal;monthVal>2;monthVal--){
                monthList.add("2024-0" + monthVal);
            }
        }

        System.out.println(String.valueOf(monthList));
        return monthList;
    }

    private List<String> listAwalMonthFilter(String month){
        List<String> monthList = new ArrayList<>();

        if(month != null){
            Integer monthVal = Integer.valueOf(month);
            monthVal = monthVal - 1;
            for(monthVal=monthVal;monthVal>2;monthVal--){
                monthList.add("2024-0" + monthVal);
            }
        }

        System.out.println(String.valueOf(monthList));
        return monthList;
    }
}
