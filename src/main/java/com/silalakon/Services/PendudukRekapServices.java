package com.silalakon.Services;

import com.silalakon.Model.DTO.PendudukRekapDtoResponse;
import com.silalakon.Model.DTO.PendudukTotalRekapDtoResponseV1;
import com.silalakon.Model.PendudukCard;
import com.silalakon.Model.PendudukRekap;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface PendudukRekapServices {

    PendudukRekap submitData(PendudukCard data);
    PendudukRekap rollbackData(PendudukCard data);
    List<PendudukRekapDtoResponse> findByDate (Pageable pageable, Map<String, String> params);
    List<PendudukTotalRekapDtoResponseV1> findRekapTotalPendudukByDate (Pageable pageable, Map<String, String> params);

}
