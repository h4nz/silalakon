package com.silalakon.Services;

import com.silalakon.Model.DTO.*;
import com.silalakon.Model.PendudukCard;
import com.silalakon.Model.PendudukRekap;
import com.silalakon.Repository.PendudukCardDaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.Local;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.aggregation.ConditionalOperators;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.naming.BinaryRefAddr;
import javax.xml.crypto.Data;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;


@Service
@Transactional
public class PendudukCardServiceImpl implements PendudukCardService{

    @Autowired
    PendudukCardDaoService pendudukCardDaoService;

    @Autowired
    PendudukRekapServices rekapServices;


    @Override
    public PendudukCard sumbitData(PendudukCard data) {
        pendudukCardDaoService.submit(data);
        rekapServices.submitData(data);
        return data;
    }

    @Override
    public PendudukCard removeData(Map<String, String> params) {
        String id = params.get("id");

        System.out.println("Id = " + id);
        Optional<PendudukCard> pendudukCard = pendudukCardDaoService.findById(id);
        if(pendudukCard.isPresent()){
            rekapServices.rollbackData(pendudukCard.get());
            pendudukCardDaoService.removeData(pendudukCard.get());
        }


        return null;
    }

    @Override
    public List<PendudukCardDtoResponse> findAllData(Pageable pageable, Map<String, String> params) {

        List<PendudukCard> listResult = new ArrayList<>();
        List<PendudukCardDtoResponse> cardDtoResponse = new ArrayList<>();

        String month = params.get("month");
        String citizen = params.get("citizen");
        String desa = params.get("desa");

        if(month == null) {
            return  null;
        }

        month = getCurrenYear() + "-" + mapperForMonthToNumber(params.get("month"));
        if(month != null && citizen != null && desa !=null){
            listResult = pendudukCardDaoService.findAllDataOneMonthCitizenDesa(month,citizen,desa);
        }else if (month != null && citizen != null && desa == null){
            listResult = pendudukCardDaoService.findAllDataOneMonthCitizen(month,citizen);
        }else if (month != null && citizen == null && desa != null){
            listResult = pendudukCardDaoService.findAllDataOneMonthDesa(month,desa);
        }else {
            listResult = pendudukCardDaoService.findAllDataOneMonth(month);
        }


        cardDtoResponse = listResult.stream().map(this::convertResponseToDto).collect(Collectors.toList());

        return cardDtoResponse;
    }

    @Override
    public List<PendudukRekapMigrasiDtoResponse> findByDateRekapMigrasi(Pageable pageable, Map<String, String> params) {

        List<PendudukCard> cardList = new ArrayList<>();
        List<PendudukRekapMigrasiDtoResponse> responseList = new ArrayList<>();

        String month = params.get("month");
        String citizen = params.get("citizen");

        month = getCurrenYear() + "-" + mapperForMonthToNumber(params.get("month"));

        if(citizen == null){
            cardList = pendudukCardDaoService.findAllDataOneMonth(month);
        }else{
            cardList  = pendudukCardDaoService.findAllDataOneMonthCitizen(month,citizen);
        }

        List<String> daftarDesa = new ArrayList<>();
        daftarDesa.add("COMPRENG");
        daftarDesa.add("MEKARJAYA");
        daftarDesa.add("KALENSARI");
        daftarDesa.add("JATIREJA");
        daftarDesa.add("KIARASARI");
        daftarDesa.add("SUKATANI");
        daftarDesa.add("SUKADANA");
        daftarDesa.add("JATIMULYA");

        for (String desa : daftarDesa){
            PendudukRekapMigrasiDtoResponse dtoResponse = new PendudukRekapMigrasiDtoResponse();

            dtoResponse.setDesa(desa);
            List<PendudukCard> listKetKatByDesa = cardList.stream()
                    .filter(f -> f.getDesa().equalsIgnoreCase(desa))
                    .collect(Collectors.toList());

            dtoResponse.setMigrasiMasukDLN_L(listKetKatByDesa.stream()
                    .filter( gender -> gender.getGender().equalsIgnoreCase("L")
                    && gender.getKeteranganKategori().equalsIgnoreCase("MIGRASI MASUK ANTAR KABUPATEN (DALAM PROVINSI)"))
                    .collect(Collectors.summingInt(PendudukCard::getCount)));
            dtoResponse.setMigrasiMasukDLN_P(listKetKatByDesa.stream()
                    .filter( gender -> gender.getGender().equalsIgnoreCase("P")
                            && gender.getKeteranganKategori().equalsIgnoreCase("MIGRASI MASUK ANTAR KABUPATEN (DALAM PROVINSI)"))
                    .collect(Collectors.summingInt(PendudukCard::getCount)));
            dtoResponse.setMigrasiMasukLRN_L(listKetKatByDesa.stream()
                    .filter( gender -> gender.getGender().equalsIgnoreCase("L")
                            && gender.getKeteranganKategori().equalsIgnoreCase("MIGRASI MASUK ANTAR PROVINSI / LUAR NEGERI"))
                    .collect(Collectors.summingInt(PendudukCard::getCount)));
            dtoResponse.setMigrasiMasukLRN_P(listKetKatByDesa.stream()
                    .filter( gender -> gender.getGender().equalsIgnoreCase("P")
                            && gender.getKeteranganKategori().equalsIgnoreCase("MIGRASI MASUK ANTAR PROVINSI / LUAR NEGERI"))
                    .collect(Collectors.summingInt(PendudukCard::getCount)));


            dtoResponse.setMigrasiKeluarDLN_L(listKetKatByDesa.stream()
                    .filter( gender -> gender.getGender().equalsIgnoreCase("L")
                            && gender.getKeteranganKategori().equalsIgnoreCase("MIGRASI KELUAR ANTAR KABUPATEN (DALAM PROPINSI)"))
                    .collect(Collectors.summingInt(PendudukCard::getCount)));
            dtoResponse.setMigrasiKeluarDLN_P(listKetKatByDesa.stream()
                    .filter( gender -> gender.getGender().equalsIgnoreCase("P")
                            && gender.getKeteranganKategori().equalsIgnoreCase("MIGRASI KELUAR ANTAR KABUPATEN (DALAM PROPINSI)"))
                    .collect(Collectors.summingInt(PendudukCard::getCount)));
            dtoResponse.setMigrasiKeluarLRN_L(listKetKatByDesa.stream()
                    .filter( gender -> gender.getGender().equalsIgnoreCase("L")
                            && gender.getKeteranganKategori().equalsIgnoreCase("MIGRASI KELUAR ANTAR PROVINSI / LUAR NEGERI"))
                    .collect(Collectors.summingInt(PendudukCard::getCount)));
            dtoResponse.setMigrasiKeluarLRN_P(listKetKatByDesa.stream()
                    .filter( gender -> gender.getGender().equalsIgnoreCase("P")
                            && gender.getKeteranganKategori().equalsIgnoreCase("MIGRASI KELUAR ANTAR PROVINSI / LUAR NEGERI"))
                    .collect(Collectors.summingInt(PendudukCard::getCount)));

            dtoResponse.setMigrasiMasukDLN_JML(dtoResponse.getMigrasiMasukDLN_L() + dtoResponse.getMigrasiMasukDLN_P());
            dtoResponse.setMigrasiMasukLRN_JML(dtoResponse.getMigrasiMasukLRN_L() + dtoResponse.getMigrasiMasukLRN_P());

            dtoResponse.setMigrasiKeluarDLN_JML(dtoResponse.getMigrasiKeluarDLN_L() + dtoResponse.getMigrasiKeluarDLN_P());
            dtoResponse.setMigrasiKeluarLRN_JML(dtoResponse.getMigrasiKeluarLRN_L() + dtoResponse.getMigrasiKeluarLRN_P());

            dtoResponse.setJmlMasuk(dtoResponse.getMigrasiMasukDLN_JML() + dtoResponse.getMigrasiMasukLRN_JML());
            dtoResponse.setJmlKeluar(dtoResponse.getMigrasiKeluarDLN_JML() + dtoResponse.getMigrasiKeluarLRN_JML());

            responseList.add(dtoResponse);
        }

        return responseList;
    }

    @Override
    public List<PendudukRekapMigrasiType2DtoResponse> findByDateRekapMigrasiType2(Pageable pageable, Map<String, String> params) {

        String tahunStatic = "2024";
        String [] citizenList = {"WNI","WNA"};
        String [] kategoriList = {"MIGRASI KELUAR ANTAR KABUPATEN (DALAM PROPINSI)",
                "MIGRASI KELUAR ANTAR PROVINSI / LUAR NEGERI",
                "MIGRASI MASUK ANTAR KABUPATEN (DALAM PROVINSI)",
                "MIGRASI MASUK ANTAR PROVINSI / LUAR NEGERI"};

        int month = 0;
        int valCount;

        List<PendudukRekapMigrasiType2DtoResponse> result = new ArrayList<>();
        List<PendudukCard> pendudukCard = pendudukCardDaoService.findAllDataDesaOnlyForMutasi("");

        System.out.println("Ini Db " +  String.valueOf(pendudukCard));

        for (String citizen : citizenList){
            for(String kategori : kategoriList){
                PendudukRekapMigrasiType2DtoResponse response = new PendudukRekapMigrasiType2DtoResponse();
                response.setCitizen(citizen);
                response.setKategori(kategori);


                for (month = 1; month <= 12 ; month++){
                    String monthSelected = String.valueOf(month);
                    System.out.println ("bulan " + tahunStatic+"-"+ String.valueOf(month));
                    List<PendudukCard> data = (List<PendudukCard>) pendudukCard.stream()
                            .filter(val -> val.getTglCreated().equalsIgnoreCase(tahunStatic+"-0"+ monthSelected)
                                    && val.getCitizen().equalsIgnoreCase(citizen)
                                    && val.getKeteranganKategori().equalsIgnoreCase(kategori))
                            .collect(Collectors.toList())
                            ;

                    valCount = data != null ? (int) data.stream().count() :0;

                    switch (month){
                        case 1:
                            response.setJanuari(valCount);
                            break;
                        case 2:
                            response.setFebruari(valCount);
                            break;
                        case 3:
                            response.setMaret(valCount);
                            break;
                        case 4:
                            response.setApril(valCount);
                            break;
                        case 5:
                            response.setMei(valCount);
                            break;
                        case 6:
                            response.setJuni(valCount);
                            break;
                        case 7:
                            response.setJuli(valCount);
                            break;
                        case 8:
                            response.setAgustus(valCount);
                            break;
                        case 9:
                            response.setSeptember(valCount);
                            break;
                        case 10:
                            response.setOktober(valCount);
                            break;
                        case 11:
                            response.setNovember(valCount);
                            break;
                        case 12:
                            response.setDesember(valCount);
                            break;
                    }
                }

                response.setTotal(0);
                response.setKet("");
                result.add(response);
            }
        }
        return result;
    }

    @Override
    public List<PendudukKelompokUmurResponse> findAllDataRekapByAgeCategory(Map<String, String> params) {

        Date date = new Date();
        String monthParams = params.get("month");

        if (params.get("month") == null){
            return  null;
        }

        monthParams = "2024-" + mapperForMonthToNumber(params.get("month"));

        List<String> montListFilter = new ArrayList<>();
        montListFilter.add(monthParams);


        List<PendudukCardDtoResponse> pendudukCards  = pendudukCardDaoService.findAllDataDesaExceptDatePindah(montListFilter).stream()
                .map(this::convertResponseToDto).collect(Collectors.toList());



        for(PendudukCardDtoResponse rekap : pendudukCards){
            Long age  = date.getTime() - rekap.getTglLahir().getTime();
            Date currentAge = new Date(age);

            LocalDate currentDate = LocalDate.now();
            LocalDate dob = rekap.getTglLahir().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            rekap.setCurrentAge(Period.between(dob,currentDate).getYears());
            rekap.setCount(1);
        }

        List<PendudukKelompokUmurResponse> responseList = new ArrayList<>();
        for(String desa : getListDesa()){
            PendudukKelompokUmurResponse kelompokUmurResponse = new PendudukKelompokUmurResponse();
            kelompokUmurResponse.setDesa(desa);
            kelompokUmurResponse.setEmpatL(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                    && data.getGender().equalsIgnoreCase("PRIA")
                    && data.getCurrentAge() <= 4)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setEmpatLP(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("WANITA")
                            && data.getCurrentAge() <= 4)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setEmpatJ(kelompokUmurResponse.getEmpatL() + kelompokUmurResponse.getEmpatLP());


            kelompokUmurResponse.setLimaL(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("PRIA")
                            && data.getCurrentAge() <= 9 && data.getCurrentAge() > 4)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setLimaP(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("WANITA")
                            && data.getCurrentAge() <= 9 && data.getCurrentAge() > 4)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setLimaJ(kelompokUmurResponse.getLimaL() + kelompokUmurResponse.getLimaP());

            kelompokUmurResponse.setTenL(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("PRIA")
                            && data.getCurrentAge() <= 14 && data.getCurrentAge() > 9)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setTenP(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("WANITA")
                            && data.getCurrentAge() <= 14 && data.getCurrentAge() > 9)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setTenJ(kelompokUmurResponse.getTenL() + kelompokUmurResponse.getTenP());

            kelompokUmurResponse.setFifteenL(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("PRIA")
                            && data.getCurrentAge() <= 19 && data.getCurrentAge() > 14)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setFifteenP(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("WANITA")
                            && data.getCurrentAge() <= 19 && data.getCurrentAge() > 14)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setFifteenJ(kelompokUmurResponse.getFifteenL()+kelompokUmurResponse.getFifteenP());

            kelompokUmurResponse.setTwentyL(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("PRIA")
                            && data.getCurrentAge() <= 24 && data.getCurrentAge() > 19)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setTwentyP(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("WANITA")
                            && data.getCurrentAge() <= 24 && data.getCurrentAge() > 19)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setTwentyJ(kelompokUmurResponse.getTwentyL()+kelompokUmurResponse.getTwentyP());

            kelompokUmurResponse.setDuaLimaL(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("PRIA")
                            && data.getCurrentAge() <= 29 && data.getCurrentAge() > 24)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setDuaLimaP(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("WANITA")
                            && data.getCurrentAge() <= 29 && data.getCurrentAge() > 24)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setDuaLimaJ(kelompokUmurResponse.getDuaLimaL()+kelompokUmurResponse.getDuaLimaP());

            kelompokUmurResponse.setTigaempatL(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("PRIA")
                            && data.getCurrentAge() <= 34 && data.getCurrentAge() > 29)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setTigaempatP(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("WANITA")
                            && data.getCurrentAge() <= 34 && data.getCurrentAge() > 29)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setTigaempatJ(kelompokUmurResponse.getTigaempatL()+kelompokUmurResponse.getTigaempatP());

            kelompokUmurResponse.setTigalimaL(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("PRIA")
                            && data.getCurrentAge() <= 39 && data.getCurrentAge() > 34)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setTigalimaP(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("WANITA")
                            && data.getCurrentAge() <= 39 && data.getCurrentAge() > 34)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setTigalimaJ(kelompokUmurResponse.getTigalimaL()+kelompokUmurResponse.getTigalimaP());

            kelompokUmurResponse.setFourtyL(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("PRIA")
                            && data.getCurrentAge() <= 44 && data.getCurrentAge() > 39)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setFourtyP(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("WANITA")
                            && data.getCurrentAge() <= 44 && data.getCurrentAge() > 39)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setFourtyJ(kelompokUmurResponse.getFourtyL()+kelompokUmurResponse.getFourtyP());

            kelompokUmurResponse.setEmpatLimaL(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("PRIA")
                            && data.getCurrentAge() <= 49 && data.getCurrentAge() > 44)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setEmpatLimaP(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("WANITA")
                            && data.getCurrentAge() <= 49 && data.getCurrentAge() > 44)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setEmpatLimaJ(kelompokUmurResponse.getEmpatLimaL()+kelompokUmurResponse.getEmpatLimaP());

            kelompokUmurResponse.setFivetyL(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("PRIA")
                            && data.getCurrentAge() <= 54 && data.getCurrentAge() > 49)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setFivetyP(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("WANITA")
                            && data.getCurrentAge() <= 54 && data.getCurrentAge() > 49)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setFivetyJ(kelompokUmurResponse.getFivetyL()+kelompokUmurResponse.getFivetyP());

            kelompokUmurResponse.setLimalimaL(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("PRIA")
                            && data.getCurrentAge() <= 59 && data.getCurrentAge() > 54)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setLimalimaP(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("WANITA")
                            && data.getCurrentAge() <= 59 && data.getCurrentAge() > 54)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setLimalimaJ(kelompokUmurResponse.getLimalimaL()+kelompokUmurResponse.getLimalimaP());

            kelompokUmurResponse.setSixtyL(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("PRIA")
                            && data.getCurrentAge() >= 60)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setSixtyP(pendudukCards.stream()
                    .filter(data -> data.getDesa().equalsIgnoreCase(desa)
                            && data.getGender().equalsIgnoreCase("WANITA")
                            && data.getCurrentAge() >= 60)
                    .map(x -> x.getCount())
                    .reduce(0, Integer::sum));
            kelompokUmurResponse.setSixtyJ(kelompokUmurResponse.getSixtyL()+kelompokUmurResponse.getSixtyP());


            responseList.add(kelompokUmurResponse);
        }

        return responseList;
    }

    private PendudukCardDtoResponse convertResponseToDto(PendudukCard response) {

        PendudukCardDtoResponse entity = new PendudukCardDtoResponse();

        entity.setId(String.valueOf(response.getId()));
        entity.setDesa(response.getDesa());
        entity.setNama(response.getNama());
        entity.setNoktp(response.getNoktp());

        switch (response.getGender()){
            case "P":
                entity.setGender("WANITA");
                break;
            case "L":
                entity.setGender("PRIA");
                break;
            default:
                entity.setGender("-");
                break;
        }

        entity.setTempatLahir(response.getTempatLahir());
        entity.setTglLahir(response.getTglLahir());
        entity.setTglLahirDb(getFormatedDateToString((entity.getTglLahir() != null ? entity.getTglLahir() : new Date())));
        entity.setCitizen(response.getCitizen());
        entity.setStatusDalamKeluarga(response.getStatusDalamKeluarga());
        entity.setAlamat(response.getAlamat());
        entity.setKategoriCard(response.getKategoriCard());
        entity.setKeteranganKategori(response.getKeteranganKategori());
        entity.setTglCreated(getFormatedDateToString(response.getTglCard()));

        return entity;
    }

    private String mapperForMonthToNumber(String month){
        String result="";

        switch(month){
            case "Januari":
                result = "01";
                break;
            case"Februari":
                result = "02";
                break;
            case "Maret":
                result = "03";
                break;
            case "April":
                result = "04";
                break;
            case "Mei":
                result = "05";
                break;
            case "Juni":
                result = "06";
                break;
            case "Juli":
                result = "07";
                break;
            case "Agustus":
                result = "08";
                break;
            case "September":
                result = "09";
                break;
            case "Oktober":
                result = "10";
                break;
            case "November":
                result = "11";
                break;
            case"Desember":
                result = "12";
                break;
        }

        return result;
    }

    private String getCurrenYear(){
        Date date = new Date();
        String modifiedDate= new SimpleDateFormat("yyyy").format(date);

        return  modifiedDate;
    }

    private String getFormatedDateToString(Date date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String modifiedDate= dateFormat.format(date);

        return  modifiedDate;
    }

    private PendudukCard mapperPendudukCardCountKategoriCard(PendudukCard data){

        PendudukCard result = new PendudukCard();

        result.setDesa(data.getDesa());
        result.setNama(data.getNama());
        result.setCount(data.getCount());
        result.setCitizen(data.getCitizen());
        result.setKeteranganKategori(data.getKeteranganKategori());

        PendudukRekapMigrasiDtoResponse resultDto = new PendudukRekapMigrasiDtoResponse();

        resultDto.setDesa(data.getDesa());
        resultDto.setCitizen(data.getCitizen());
        resultDto.setKategoriPeristiwa(data.getKeteranganKategori());

        return  result;
    }

    private Integer calculateAge(Date dob, Date date){
        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        int d1 = Integer.parseInt(formatter.format(dob));
        int d2 = Integer.parseInt(formatter.format(date));
        int age = (d2 - d1) / 10000;
        return age;
    }

    private List<String> getListDesa(){
        List<String> daftarDesa = new ArrayList<>();
        daftarDesa.add("COMPRENG");
        daftarDesa.add("MEKARJAYA");
        daftarDesa.add("KALENSARI");
        daftarDesa.add("JATIREJA");
        daftarDesa.add("KIARASARI");
        daftarDesa.add("SUKATANI");
        daftarDesa.add("SUKADANA");
        daftarDesa.add("JATIMULYA");

        return daftarDesa;
    }
}
