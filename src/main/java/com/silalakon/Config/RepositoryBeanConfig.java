package com.silalakon.Config;

import com.silalakon.Repository.PendudukCardDaoService;
import com.silalakon.Repository.PendudukCardDaoServiceImpl;
import com.silalakon.Repository.PendudukRekapDaoService;
import com.silalakon.Repository.PendudukRekapDaoServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RepositoryBeanConfig {

    @Bean
    public PendudukCardDaoService pendudukCardDaoService (){return new PendudukCardDaoServiceImpl();
    }

    @Bean
    public PendudukRekapDaoService pendudukRekapDaoService(){return new PendudukRekapDaoServiceImpl();
    }
}
