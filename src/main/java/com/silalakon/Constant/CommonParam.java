package com.silalakon.Constant;

public class CommonParam {

    public final static String GENDER_LAKI = "L";
    public final static String GENDER_WANITA = "P";

    public final static String PENDUDUK_CARD_CATEGORY_LAHIR = "LAHIR";
    public final static String PENDUDUK_CARD_CATEGORY_MATI = "MATI";
    public final static String PENDUDUK_CARD_CATEGORY_PINDAH = "PINDAH";
    public final static String PENDUDUK_CARD_CATEGORY_DATANG = "DATANG";
}
