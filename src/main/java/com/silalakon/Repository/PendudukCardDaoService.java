package com.silalakon.Repository;

import com.silalakon.Model.PendudukCard;

import java.util.List;
import java.util.Optional;

public interface PendudukCardDaoService {

    Optional<PendudukCard> findById (String id);
    PendudukCard submit (PendudukCard data);
    PendudukCard removeData (PendudukCard data);

    List<PendudukCard> findAllDataDesaExceptDatePindah(List<String> month);
    List<PendudukCard> findAllDataOneMonthCitizenDesa(String month, String citizen, String desa);
    List<PendudukCard> findAllDataOneMonthDesa(String month,String desa);
    List<PendudukCard> findAllDataOneMonthCitizen(String month,String citizen);
    List<PendudukCard> findAllDataOneMonth(String month);
    List<PendudukCard> findAllDataDesaOnlyForMutasi(String tahun);
    List<PendudukCard> findAllDataThatAliveByVillageCitizen(String tglFrom, String tglTo, String village, String citizen) ;
    List<PendudukCard> findAllDataThatLeaveByVillageCitizen(String tglFrom, String tglTo, String village, String citizen) ;
    List<PendudukCard> findAllDataThatAliveByVillageCitizenV2(List<String> monthIn, String village, String citizen) ;
    List<PendudukCard> findAllDataThatLeaveByVillageCitizenV2(List<String> monthIn, String village, String citizen) ;

}
