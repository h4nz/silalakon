package com.silalakon.Repository;

import com.silalakon.Model.PendudukCard;
import com.silalakon.Model.PendudukRekap;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class PendudukCardDaoServiceImpl implements PendudukCardDaoService{

    @Autowired
    PendudukCardRepo pendudukCardRepo;


    @Override
    public Optional<PendudukCard> findById(String id) {
        return pendudukCardRepo.findById(new ObjectId(id));
//        return pendudukCardRepo.findByIdString(id);
    }

    @Override
    public PendudukCard submit(PendudukCard data) {
        data.setTglCreated(getCurrenBulanDate(data.getTglCard()));
        data.setCount(1);
        pendudukCardRepo.save(data);
        return data;
    }

    @Override
    public PendudukCard removeData(PendudukCard data) {
        pendudukCardRepo.deleteById(data.getId());
        return null;
    }

    @Override
    public List<PendudukCard> findAllDataDesaExceptDatePindah(List<String> month) {
        return pendudukCardRepo.findAllDataExceptDateAndPindahByMonthV1(month);
    }

    @Override
    public List<PendudukCard> findAllDataOneMonthCitizenDesa(String month, String citizen, String desa) {
        return pendudukCardRepo.findAllDataOneMonthCitizenDesaV1(month,citizen,desa);
    }

    @Override
    public List<PendudukCard> findAllDataOneMonthDesa(String month, String desa) {
        return pendudukCardRepo.findAllDataOneMonthDesaV1(month,desa);
    }

    @Override
    public List<PendudukCard> findAllDataOneMonthCitizen(String month, String citizen) {
        return pendudukCardRepo.findAllDataOneMonthCitizenV1(month,citizen);
    }

    @Override
    public List<PendudukCard> findAllDataOneMonth(String month) {
        return pendudukCardRepo.findAllDataOneMonth(month);
    }

    @Override
    public List<PendudukCard> findAllDataDesaOnlyForMutasi(String tahun) {
        return pendudukCardRepo.findAllDataForMutasi();
    }

    @Override
    public List<PendudukCard> findAllDataThatAliveByVillageCitizen(String tglFrom, String tglTo, String village, String citizen) {
        return pendudukCardRepo.findAllDataThatAliveByVillageCitizen(tglTo,village,citizen);
    }

    @Override
    public List<PendudukCard> findAllDataThatLeaveByVillageCitizen(String tglFrom, String tglTo, String village, String citizen) {
        return pendudukCardRepo.findAllDataThatLeaveByVillageCitizen(tglTo,village,citizen);
    }

    @Override
    public List<PendudukCard> findAllDataThatAliveByVillageCitizenV2(List<String> monthIn, String village, String citizen) {
        return pendudukCardRepo.findAllDataThatAliveByVillageCitizenV2(monthIn,village,citizen);
    }

    @Override
    public List<PendudukCard> findAllDataThatLeaveByVillageCitizenV2(List<String> monthIn, String village, String citizen) {
        return pendudukCardRepo.findAllDataThatLeaveByVillageCitizenV2(monthIn,village,citizen);
    }


    private String getCurrenBulanDate(Date date){
        String modifiedDate= new SimpleDateFormat("yyyy-MM").format(date);

        return  modifiedDate;
    }
}
