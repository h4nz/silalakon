package com.silalakon.Repository;

import com.silalakon.Model.PendudukCard;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PendudukCardRepo extends MongoRepository<PendudukCard, ObjectId> {

    @Query(value = "{'_id' : ObjectId( ?0 )")
    Optional<PendudukCard> findByIdString(String id);

    @Query(value = "{'kategoriCard' : { $nin : ['PINDAH','MATI']}}")
    List<PendudukCard> findAllDataExceptDateAndPindah();

    @Query(value = "{'kategoriCard' : { $nin : ['PINDAH','MATI']}, 'tglCreated' : { $nin : ?0 } }")
    List<PendudukCard> findAllDataExceptDateAndPindahByMonthV1(List<String> month);

    @Query(value = "{ 'tglCreated' : ?0, 'citizen' : ?1 'desa' :  ?2 }")
    List<PendudukCard> findAllDataOneMonthCitizenDesaV1(String month, String citizen, String desa);

    @Query(value = "{ 'tglCreated' : ?0, 'desa' :  ?1 }")
    List<PendudukCard> findAllDataOneMonthDesaV1(String month, String desa);

    @Query(value = "{ 'tglCreated' : ?0, 'citizen' :  ?1 }")
    List<PendudukCard> findAllDataOneMonthCitizenV1(String month, String citizen);

    @Query(value = "{ 'tglCreated' : ?0 }")
    List<PendudukCard> findAllDataOneMonth(String month);

    @Query(value = "{'kategoriCard' : { $in : ['PINDAH','DATANG']}}")
    List<PendudukCard> findAllDataForMutasi();

    @Query(value = "{ 'tglCard' : { $gte: {$date:'2024-03-01'} ,$lte: {$date: ?0}}, 'desa' : ?1, 'citizen' : ?2 , 'kategoriCard' : { $in : ['LAHIR','DATANG'] }  }")
    List<PendudukCard> findAllDataThatAliveByVillageCitizen(String tglTo, String desa, String citizen);

    @Query(value = "{ 'tglCard' : { $gte: {$date:'2024-03-01'} ,$lte: {$date: ?0}}, 'desa' : ?1, 'citizen' : ?2 , 'kategoriCard' : { $in : ['PINDAH','MATI'] }  }")
    List<PendudukCard> findAllDataThatLeaveByVillageCitizen(String tglTo, String desa, String citizen);

    @Query(value = "{ 'tglCreated' : { $in: ?0 } , 'desa' : ?1, 'citizen' : ?2 , 'kategoriCard' : { $in : ['LAHIR','DATANG'] }  }")
    List<PendudukCard> findAllDataThatAliveByVillageCitizenV2(List<String> month, String desa, String citizen);

    @Query(value = "{ 'tglCreated' : { $in: ?0 } , 'desa' : ?1, 'citizen' : ?2 , 'kategoriCard' : { $in : ['PINDAH','MATI'] }  }")
    List<PendudukCard> findAllDataThatLeaveByVillageCitizenV2(List<String> monthIn, String desa, String citizen);

}
