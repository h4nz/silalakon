package com.silalakon.Repository;

import com.silalakon.Model.PendudukRekap;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PendudukRekapRepo extends MongoRepository<PendudukRekap, ObjectId> {

    @Query(value = "{ 'bulanRekap' : ?0, 'desa' : ?1, 'citizen' : ?2 }")
    Optional<PendudukRekap> findByDateMonthVillageCitizen(String date, String desa, String citizen);

    @Query(value = "{ 'bulanRekap' : ?0, 'citizen' : ?1 }")
    List<PendudukRekap> findByDateMonthCitizen(String date, String citizen);

    @Query(value = "{ 'bulanRekap' : ?0 }")
    List<PendudukRekap> findByDateMonth(String date);

    @Query(value = "{ 'bulanRekap' : { $in : ?0 }, 'desa' : ?1, 'citizen' : ?2 , 'kategoriCard' : { $in : ['LAHIR','DATANG'] }  }")
    List<PendudukRekap> findAllDataThatAliveByVillageCitizen(List<String> month, String desa, String citizen);
}
