package com.silalakon.Repository;

import com.silalakon.Model.PendudukCard;
import com.silalakon.Model.PendudukRekap;

import java.util.List;
import java.util.Optional;

public interface PendudukRekapDaoService {

    PendudukRekap submit(PendudukRekap rekap);
    Optional<PendudukRekap> findByDateDesaCitizen(String Date, String desa, String citizen);

    List<PendudukRekap> findAllDataByMonthCitizen(String month, String citizen);
    List<PendudukRekap> findAllDataByMonth(String month);
    List<PendudukRekap> findAllDataThatAliveByVillage(List<String> month);


}
