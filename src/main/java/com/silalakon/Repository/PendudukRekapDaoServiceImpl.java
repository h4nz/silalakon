package com.silalakon.Repository;

import com.silalakon.Model.PendudukRekap;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

public class PendudukRekapDaoServiceImpl implements PendudukRekapDaoService{

    @Autowired
    PendudukRekapRepo rekapRepo;

    @Override
    public PendudukRekap submit(PendudukRekap rekap) {
        return rekapRepo.save(rekap);
    }

    @Override
    public Optional<PendudukRekap> findByDateDesaCitizen(String date, String desa, String citizen) {
        return rekapRepo.findByDateMonthVillageCitizen(date,desa,citizen);
    }

    @Override
    public List<PendudukRekap> findAllDataByMonthCitizen(String month, String citizen) {
        return rekapRepo.findByDateMonthCitizen(month,citizen);
    }

    @Override
    public List<PendudukRekap> findAllDataByMonth(String month) {
        return rekapRepo.findByDateMonth(month);
    }

    @Override
    public List<PendudukRekap> findAllDataThatAliveByVillage(List<String> month) {
        return null;
    }
}
