package com.silalakon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SilalakonApplication {

	public static void main(String[] args) {
		SpringApplication.run(SilalakonApplication.class, args);
	}

}
