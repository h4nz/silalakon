package com.silalakon.Controller;

import com.silalakon.Model.DTO.*;
import com.silalakon.Model.PendudukCard;
import com.silalakon.Model.PendudukRekap;
import com.silalakon.Services.PendudukCardService;
import com.silalakon.Services.PendudukRekapServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/silalakon")
public class PendudukCardController {

    @Autowired
    PendudukCardService service;

    @Autowired
    PendudukRekapServices rekapServices;

    @RequestMapping(value = "/submitV1",method = RequestMethod.POST)
    public ResponseEntity<PendudukCard> submit(@RequestBody PendudukCard data) throws Exception {
        try{
            return ResponseEntity.ok().body(this.service.sumbitData(data));
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    @RequestMapping(value = "/removeV1",method = RequestMethod.PUT)
    public ResponseEntity<PendudukCard> removeData(@RequestParam Map<String, String> params) throws Exception {
//        try{
            return ResponseEntity.ok().body(this.service.removeData(params));
//        }catch (Exception e){
//            throw new Exception(e.getMessage());
//        }
    }

    @RequestMapping(value = "/findv1",method = RequestMethod.GET)
    public ResponseEntity<List<PendudukCardDtoResponse>> getAllData(@RequestParam Map<String, String> params) throws Exception {
//        try{
            Pageable pageable  = PageRequest.of(0,1000, Sort.by("createdTime"));
            return ResponseEntity.ok().body(this.service.findAllData(pageable,params));
//        }catch (Exception e){
//            throw new Exception(e.getMessage());
//        }
    }

    @RequestMapping(value = "/findMutasiV1",method = RequestMethod.GET)
    public ResponseEntity<List<PendudukRekapDtoResponse>> getAllDataRekapMutasi(@RequestParam Map<String, String> params) throws Exception {
//        try{
            Pageable pageable  = PageRequest.of(0,1000, Sort.by("createdTime"));
            return ResponseEntity.ok().body(this.rekapServices.findByDate(pageable,params));
//        }catch (Exception e){
//            throw new Exception(e.getMessage());
//        }
    }

    @RequestMapping(value = "/findRekapMigrasiV1",method = RequestMethod.GET)
    public ResponseEntity<List<PendudukRekapMigrasiDtoResponse>> getAllDataRekapMigrasi(@RequestParam Map<String, String> params) throws Exception {
//        try{
            Pageable pageable  = PageRequest.of(0,1000, Sort.by("createdTime"));
            return ResponseEntity.ok().body(this.service.findByDateRekapMigrasi(pageable,params));
//        }catch (Exception e){
//            throw new Exception(e.getMessage());
//        }
    }

    @RequestMapping(value = "/findRekapMigrasiType2V1",method = RequestMethod.GET)
    public ResponseEntity<List<PendudukRekapMigrasiType2DtoResponse>> getAllDataRekapMigrasiType2(@RequestParam Map<String, String> params) throws Exception {
//        try{
        Pageable pageable  = PageRequest.of(0,1000, Sort.by("createdTime"));
        return ResponseEntity.ok().body(this.service.findByDateRekapMigrasiType2(pageable,params));
//        }catch (Exception e){
//            throw new Exception(e.getMessage());
//        }
    }

    @RequestMapping(value = "/findRekapTotalPendudukV1",method = RequestMethod.GET)
    public ResponseEntity<List<PendudukTotalRekapDtoResponseV1>> getAllTotalPendudukV1(@RequestParam Map<String, String> params) throws Exception {
//        try{
        Pageable pageable  = PageRequest.of(0,1000, Sort.by("createdTime"));
        return ResponseEntity.ok().body(this.rekapServices.findRekapTotalPendudukByDate(pageable,params));
//        }catch (Exception e){
//            throw new Exception(e.getMessage());
//        }
    }

    @RequestMapping(value = "/findRekapKelompokUmurV1",method = RequestMethod.GET)
    public ResponseEntity<List<PendudukKelompokUmurResponse>> getTotalKelompokUmur(@RequestParam Map<String, String> params) throws Exception {
//        try{
        return ResponseEntity.ok().body(this.service.findAllDataRekapByAgeCategory(params));
//        }catch (Exception e){
//            throw new Exception(e.getMessage());
//        }
    }
}
